import os
import random
import pygame
from pygame import mixer

pygame.init()
pygame.display.set_caption("Deep Sea Death")
screen = pygame.display.set_mode((1000, 1000))

pl = 0
round_no = 0
# images of player 1 and 2
p1img = pygame.image.load("P1.png")
p2img = pygame.image.load("P2.png")

# image of static ostacle
mine = pygame.image.load("sea.png")

# image of moving obstacle
bomb = pygame.image.load("bomb.png")

os.environ["SDL_VIDEO_CENTERED"] = "1"

color = (128, 128, 128)
# score bonus based on time overall
score_1_bonus = [0] * 100
score_2_bonus = [0] * 100

# score on basis of obstacles crossed for each round
score_1 = [0] * 100
score_2 = [0] * 100

# final scores total
score_1_final = 0
score_2_final = 0

font = pygame.font.Font("freesansbold.ttf", 15)

# co-ordinates of players
p1x = 500
p1y = 16
p2x = 500
p2y = 16

# initializing moving obstacles spawn location
moving_x = []
for i in range(14):
    moving_x.append(random.randint(32, 900))
moving_y = [75, 90, 250, 300, 325, 460, 550, 660, 700, 750, 870, 890, 930, 915]

# initializing static obstacle spawn location
ob_static = [
    (random.randint(32, 900), 390),
    (random.randint(32, 900), 610),
    (random.randint(32, 900), 610),
    (random.randint(32, 900), 610),
    (random.randint(32, 900), 830),
    (random.randint(32, 900), 830),
    (random.randint(32, 900), 830),
]

# speed of moving obstacles
moving_speed_1 = 2
moving_speed_2 = 2

# backgtound music
mixer.music.load("background.wav")
mixer.music.play(-1)

# collision sound
collision = mixer.Sound("explosion.wav")

# the function to display the player score and time penalty
def show_score(x, y, player_no, score, bonus):
    score = font.render(
        "Player "
        + str(player_no)
        + " : "
        + str(score)
        + " "
        + "Time Penalty : "
        + str(bonus),
        True,
        (255, 255, 255),
    )
    screen.blit(score, (x, y))


# the function to show the player image
def player(img, x, y):
    screen.blit(img, (x, y))


# function to display static obstacle
def static(pos):
    screen.blit(mine, (pos[0], pos[1]))


# function to display moving obstacle
def moving(x, y):
    screen.blit(bomb, (x, y))


clock = pygame.time.Clock()
close = False
