									This is my Pygame DEEPSEA DEATH

In this game the objective is to reach the bottom of the ocean floor while avoiding the mines and the torpedoes.

The player starts at the top of the screen.Each time a player dies or reaches the bottom, the next player is spawned at the top.

A round consists of both players playing once.

The game runs till 100 rounds or till you press the escape key.

CONTROLS : 

Player 1 is controlled using the arrow keys.
Player 2 is controlled using w,a,s,d keys.
(you know what they do.)

Press ESC to end game and see the final score and check who won.

SCORING : 

For each moving obstacle you cross, you will gain 10 points, and for fixed obstacles, you will gain 5 points for crossing them.
Be warned, going backwards will make you loose points too.

On reaching the bottom, you get a bonus of 100 points!!

You also accrue a time penalty, 15.5 negative points per second you take to complete the round OR DIE!!

The final score is calculated by adding the scores and time penalty of all rounds played.

Whoever scores the maximum points wins.
