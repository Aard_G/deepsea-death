#! /usr/bin/env python
import os
import random
import pygame
from pygame import mixer
from config import *


while not close:
    # to fill the screen with the background color at start of every loop
    screen.fill((0, 128, 255))
    for event in pygame.event.get():
        # if cross is pressed loop is broken
        if event.type == pygame.QUIT:
            close = True
    # detection of key press
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_UP]:
        p1y -= 2
    if pressed[pygame.K_DOWN]:
        p1y += 2
    if pressed[pygame.K_LEFT]:
        p1x -= 2
    if pressed[pygame.K_RIGHT]:
        p1x += 2
    if pressed[pygame.K_w]:
        p2y -= 2
    if pressed[pygame.K_s]:
        p2y += 2
    if pressed[pygame.K_a]:
        p2x -= 2
    if pressed[pygame.K_d]:
        p2x += 2
    if pressed[pygame.K_ESCAPE]:
        break

    # boundary of players
    if p1x < 0:
        p1x = 0
    if p1x > 960:
        p1x = 960
    if p1y < 20:
        p1y = 20
    if p1y > 960:
        p1y = 960
    if p2x < 0:
        p2x = 0
    if p2x > 960:
        p2x = 960
    if p2y < 20:
        p2y = 20
    if p2y > 960:
        p2y = 960

    # draws the partitioning rectangles
    pygame.draw.rect(screen, color, pygame.Rect(0, 0, 1000, 20))
    pygame.draw.rect(screen, color, pygame.Rect(0, 160, 1000, 50))
    pygame.draw.rect(screen, color, pygame.Rect(0, 380, 1000, 50))
    pygame.draw.rect(screen, color, pygame.Rect(0, 600, 1000, 50))
    pygame.draw.rect(screen, color, pygame.Rect(0, 820, 1000, 50))
    pygame.draw.rect(screen, color, pygame.Rect(0, 980, 1000, 50))

    # displays the round number,score of each player and the time penalty
    show_score(100, 0, 1, score_1[round_no], score_1_bonus[round_no])
    show_score(700, 0, 2, score_2[round_no], score_2_bonus[round_no])
    display_round = font.render(
        "Round number : " + str(round_no + 1), True, (255, 255, 255)
    )
    screen.blit(display_round, (400, 0))

    # this loop draws static obstacles and detects collisions
    for i in ob_static:
        static(i)

        # collision detection with static objects for player 1
        if pl == 1:
            distance_sq = ((i[0] + 16) - (p1x + 16)) ** 2 + (
                (i[1] + 16) - (p1y + 16)
            ) ** 2
            if distance_sq < 1024:
                pl += 1
                collision.play()
                distance_sq = 100000

                # randomizes initial starting position of the moving obstacles
                for i in range(14):
                    moving_x[i] = random.randint(32, 900)

                # radomizes position of the static objects
                ob_static = [
                    (random.randint(32, 900), 170),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                ]

        # collision detection with static objects for player 2
        if pl == 3:
            distance_sq = ((i[0] + 16) - (p2x + 16)) ** 2 + (
                (i[1] + 16) - (p2y + 16)
            ) ** 2
            if distance_sq < 1024:
                pl = 0
                collision.play()
                round_no += 1
                distance_sq = 100000

                # randomizes initial starting position of the moving obstacles
                for i in range(14):
                    moving_x[i] = random.randint(32, 900)

                # radomizes position of the static objects
                ob_static = [
                    (random.randint(32, 900), 170),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                ]

    # changes position of moving objects
    for i in range(14):
        if pl == 1:
            moving_x[i] -= moving_speed_1
        if pl == 3:
            moving_x[i] -= moving_speed_2
        if moving_x[i] < 0:
            moving_x[i] = 960
        moving(moving_x[i], moving_y[i])

        # collision detection with moving objects for player 1
        if pl == 1:
            distance_sq = ((moving_x[i] + 16) - (p1x + 16)) ** 2 + (
                (moving_y[i] + 16) - (p1y + 16)
            ) ** 2
            if distance_sq < 1024:
                # if player 1 dies pl is incremented to spawn player 2
                pl += 1
                collision.play()
                distance_sq = 100000

                # randomizes initial starting position of the moving obstacles
                for i in range(14):
                    moving_x[i] = random.randint(32, 900)

                # randomizes initial starting position of the static obstacles
                ob_static = [
                    (random.randint(32, 900), 170),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                ]

        # collision detection with moving objects for player 1
        if pl == 3:
            distance_sq = ((moving_x[i] + 16) - (p2x + 16)) ** 2 + (
                (moving_y[i] + 16) - (p2y + 16)
            ) ** 2
            if distance_sq < 1024:
                # if player 2 dies player 1 is spawned also the rounf ends
                pl = 0
                collision.play()
                round_no += 1
                distance_sq = 100000

                # randomizes initial starting position of the moving obstacles
                for i in range(14):
                    moving_x[i] = random.randint(32, 900)

                # randomizes initial starting position of the static obstacles
                ob_static = [
                    (random.randint(32, 900), 170),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 390),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 610),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                    (random.randint(32, 900), 830),
                ]

    # the variable pl is used to determine which player is playing
    if pl == 0:
        # the starting position of player one
        p1x = 500
        p1y = 16
        pl += 1

    if pl == 1:
        player(p1img, p1x, p1y)
        # scoring based on obstacles crossed for player 2
        if p1y > 930:
            score_1[round_no] = 155
        elif p1y > 915:
            score_1[round_no] = 150
        elif p1y > 890:
            score_1[round_no] = 140
        elif p1y > 870:
            score_1[round_no] = 130
        elif p1y > 830:
            score_1[round_no] = 120
        elif p1y > 750:
            score_1[round_no] = 115
        elif p1y > 700:
            score_1[round_no] = 105
        elif p1y > 660:
            score_1[round_no] = 95
        elif p1y > 610:
            score_1[round_no] = 85
        elif p1y > 550:
            score_1[round_no] = 80
        elif p1y > 460:
            score_1[round_no] = 70
        elif p1y > 390:
            score_1[round_no] = 60
        elif p1y > 325:
            score_1[round_no] = 55
        elif p1y > 300:
            score_1[round_no] = 45
        elif p1y > 250:
            score_1[round_no] = 35
        elif p1y > 170:
            score_1[round_no] = 25
        elif p1y > 90:
            score_1[round_no] = 20
        elif p1y > 75:
            score_1[round_no] = 10
        if p1y > 30:
            # time penalty begins increasing after the player moves
            score_1_bonus[round_no] -= 0.25

    if p1y == 960:
        # bonus for completing the lvel
        score_1[round_no] += 100
        p1x = 500
        p1y = 16
        pl += 1
        # position of every obstacle is randomized again
        for i in range(14):
            moving_x[i] = random.randint(32, 900)
        ob_static = [
            (random.randint(32, 900), 170),
            (random.randint(32, 900), 390),
            (random.randint(32, 900), 390),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 830),
            (random.randint(32, 900), 830),
            (random.randint(32, 900), 830),
        ]
        # moving speed is increased for that player every time they coomplete the level
        moving_speed_1 *= 1.5

    if pl == 2:
        # starting positoin of player 2
        p2x = 500
        p2y = 16
        pl += 1
    if pl == 3:
        player(p2img, p2x, p2y)

        # scoring based on obstacles crossed for player 2
        if p2y > 930:
            score_2[round_no] = 155
        elif p2y > 915:
            score_2[round_no] = 150
        elif p2y > 890:
            score_2[round_no] = 140
        elif p2y > 870:
            score_2[round_no] = 130
        elif p2y > 830:
            score_2[round_no] = 120
        elif p2y > 750:
            score_2[round_no] = 115
        elif p2y > 700:
            score_2[round_no] = 105
        elif p2y > 660:
            score_2[round_no] = 95
        elif p2y > 610:
            score_2[round_no] = 85
        elif p2y > 550:
            score_2[round_no] = 80
        elif p2y > 460:
            score_2[round_no] = 70
        elif p2y > 390:
            score_2[round_no] = 60
        elif p2y > 325:
            score_2[round_no] = 55
        elif p2y > 300:
            score_2[round_no] = 45
        elif p2y > 250:
            score_2[round_no] = 35
        elif p2y > 170:
            score_2[round_no] = 25
        elif p2y > 90:
            score_2[round_no] = 20
        elif p2y > 75:
            score_2[round_no] = 10
        if p2y > 30:
            score_2_bonus[round_no] -= 0.25

    if p2y == 960:
        p2x = 500
        p2y = 16
        score_2[round_no] += 100
        round_no += 1

        # position of every obstacle is randomized again
        for i in range(14):
            moving_x[i] = random.randint(32, 900)
        ob_static = [
            (random.randint(32, 900), 170),
            (random.randint(32, 900), 390),
            (random.randint(32, 900), 390),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 610),
            (random.randint(32, 900), 830),
            (random.randint(32, 900), 830),
            (random.randint(32, 900), 830),
        ]
        pl = 0

        # obstacle speed increased for player 2 on completing the level
        moving_speed_2 *= 1.5

    # To change the content of the screen on end of every loop
    pygame.display.update()
    clock.tick(60)

# This is the loop for diplaying the results at the end
close = False
while not close:
    screen.fill((0, 128, 255))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            close = True
        score_1_final = 0
        score_2_final = 0
        # summation of the score based on positionn
        for i in score_1:
            score_1_final += i
        for i in score_2:
            score_2_final += i

        # summation of score with time penalty added
        for i in score_1_bonus:
            score_1_final += i
        for i in score_2_bonus:
            score_2_final += i

    # this is to display the final score of each player and display on the screen
    font = pygame.font.Font("freesansbold.ttf", 50)
    player_1_score = font.render(
        "Player 1 scored " + str(score_1_final), True, (0, 0, 0)
    )
    player_2_score = font.render(
        "Player 2 scored " + str(score_2_final), True, (0, 0, 0)
    )
    screen.blit(player_1_score, (250, 225))
    screen.blit(player_2_score, (250, 295))

    # this block decides the winning messsage
    if score_2_final > score_1_final:
        win_message = font.render("Player 2 wins!!!", True, (25, 0, 0))
    elif score_2_final < score_1_final:
        win_message = font.render("Player 1 wins!!!", True, (25, 0, 0))
    else:
        win_message = font.render("It's a draw!!!", True, (25, 0, 0))
    # this block displays the winning message
    screen.blit(win_message, (400, 400))

    pygame.display.update()
    clock.tick(60)
